import pandas as pd
import plotly.graph_objects as go
import plotly.express as px
from sklearn.mixture import GaussianMixture
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, fbeta_score, confusion_matrix
from sklearn.model_selection import train_test_split


MACHINE_FILE_FATH = "../data/creditcard.csv"


def plot_transactions(df):
    fig = go.Figure()
    # Add traces
    fig.add_trace(go.Scatter(x=df['Time'], y=df['Amount'], mode='markers', name='Amount', marker_color=df["Class"]))

    fig.update_layout(
        title="Credit Card Transactions",
        xaxis_title="Time"
    )

    fig.show()


def print_accuracies(y_true, y_pred):
    accuracy = accuracy_score(y_true, y_pred)
    precision = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    fbeta = fbeta_score(y_true, y_pred, beta=2)
    f1 = f1_score(y_true, y_pred)
    print(f'acc: {accuracy}, precision: {precision}, recall: {recall}, f1: {f1}, f2: {fbeta}\n')
    print(confusion_matrix(y_true, y_pred))


def main():
    df = pd.read_csv(MACHINE_FILE_FATH)
    # plot_transactions(df)
    print(df.head())

    # # plotting the feature histograms w.r.t to the label
    # for f in df.columns[1:-1]:
    #     fig = px.histogram(df, x=f, color="Class")
    #     fig.show()

    #  After see histograms, choose mose useful features
    df = df[["V3", "V4", "V6", "V7", "V9", "V10", "V11", "V12", "V14", "V16", "V17", "V18", "Amount", "Class"]]

    df_frauds = df.loc[df["Class"] == 1]
    df_regular = df.loc[df["Class"] == 0]

    # split train/test regarding to time - to preserve fraud distribution
    df_regular_train_X, df_regular_test_X, df_regular_train_y, df_regular_test_y = train_test_split\
        (df_regular[df.columns.difference(['Class'])], df_regular[["Class"]], test_size=0.4, random_state=0,
         shuffle=True)
    df_frauds_X = df_frauds[df.columns.difference(['Class'])]
    df_frauds_y = df_frauds[["Class"]]

    # split no fraud data to train and validation
    X_val_no_fraus, X_test, y_val_no_fraud, y_test = train_test_split\
        (df_regular_test_X, df_regular_test_y, test_size=0.5, random_state=0,
         shuffle=True)

    # split fraud data to train and validation
    X_fraud_val, X_fraud_test, y_fraud_val, y_fraud_test = train_test_split\
        (df_frauds_X, df_frauds_y, test_size=0.5, random_state=0,
         shuffle=True)

    # validation data and test data
    X_val = pd.concat([X_val_no_fraus, X_fraud_val])
    y_val = pd.concat([y_val_no_fraud, y_fraud_val])
    X_test = pd.concat([X_test, X_fraud_test])
    y_test = pd.concat([y_test, y_fraud_test])

    # train gmm model on training
    gmm_model = GaussianMixture()
    gmm_model.fit(df_regular_train_X)

    # Train Results
    gmm_log_probs_train = gmm_model.score_samples(df_regular_train_X)
    fig = px.scatter(y=gmm_log_probs_train, color=df_regular_train_y.to_numpy().flatten(), title="Training log probabilities")
    fig.show()

    # validation Results
    gmm_log_probs_val = gmm_model.score_samples(X_val)
    fig = px.scatter(y=gmm_log_probs_val, color=y_val.to_numpy().flatten(), title="Validation log probabilities")
    fig.show()

    # find threshold according validation. ----> to maximize f2
    optimum_t = 0
    optimum_fbeta = 0
    for t in range(-18, -178, -1):
        gmm_predictions_val = [0 if l > t else 1 for l in gmm_log_probs_val]
        fbeta = fbeta_score(y_test, gmm_predictions_val, beta=2)
        if fbeta > optimum_fbeta:
            optimum_t = t
            optimum_fbeta = fbeta
    print(f'Optimum t: {optimum_t}')

    # fit GMM on both train+val(without frauds)
    train_x = pd.concat([df_regular_train_X, X_val_no_fraus])
    gmm_model.fit(train_x)

    # Test Results
    gmm_log_probs_test = gmm_model.score_samples(X_test)
    fig = px.scatter(y=gmm_log_probs_test, color=y_test.to_numpy().flatten(), title="Test log probabilities")
    fig.show()

    gmm_predictions_test = [0 if l > optimum_t else 1 for l in gmm_log_probs_test]
    print_accuracies(y_test, gmm_predictions_test)

    # Optimum
    # t: -159
    # acc: 0.9981263898860074, precision: 0.7883817427385892, recall: 0.7723577235772358, f1: 0.7802874743326489,
    # f2: 0.7755102040816326
    #
    # [[56812    51]
    #  [56   190]]


if __name__ == '__main__':
    main()
