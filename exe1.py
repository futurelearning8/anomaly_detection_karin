import pandas as pd
import plotly.graph_objects as go
from sklearn.ensemble import IsolationForest
from sklearn.preprocessing import StandardScaler


MACHINE_FILE_FATH = "../data/machine_inspection.txt"


def plot_machine_inspection(df, color=None):
    fig = go.Figure()
    # Add traces
    if color is not None:
        fig.add_trace(go.Scatter(x=df['pressure'], y=df['temp'], mode='markers', name='Temp', marker_color=color))
    else:
        fig.add_trace(go.Scatter(x=df['pressure'], y=df['temp'], mode='markers', name='Temp'))

    fig.update_layout(
        title="Machine inspection",
        xaxis_title="measures"
    )

    fig.show()


def main():
    df = pd.read_csv(MACHINE_FILE_FATH, names=['temp', 'pressure'], delim_whitespace=True)

    # 2. visualize the data
    plot_machine_inspection(df)

    # 3. a. unsupervised, b. 8% ?, c. Isolation Forest
    scaler = StandardScaler()
    # normalize the metrics
    scaler.fit(df)
    df = pd.DataFrame(scaler.transform(df), columns=df.columns)

    # IsolationForest model
    IF = IsolationForest(n_estimators=200, max_samples=256, random_state=1, contamination=0.08)
    IF.fit(df)
    IF_pred = IF.predict(df)
    plot_machine_inspection(df, color=IF_pred)

    # Test my model
    x_test = pd.DataFrame([(470, 505), (642, 634), (450, 650), (625, 400)], columns=df.columns)
    x_test = scaler.transform(x_test)
    IF_pred = IF.predict(x_test)
    print(f'IsolationForest model results for test set: {IF_pred}\n')


if __name__ == '__main__':
    main()
